var map,
	mapContacts;
function initMap() {
	if($('#map').length > 0) {
		map = new google.maps.Map(document.getElementById('map'), {
		 center: {lat: 45.0402809, lng: 38.9897303},
		  zoom: 15,
		  scrollwheel: false,
		  zoomControl: false,
		  disableDefaultUI: true, 
		});
	}	

	if($('#map-contacts').length > 0) {
		mapContacts = new google.maps.Map(document.getElementById('map-contacts'), {
		 center: {lat: 45.0402809, lng: 38.9897303},
		  zoom: 15,
		  scrollwheel: false,
		  zoomControl: false,
		  disableDefaultUI: true, 
		});
	}


};

$(function () {


	if($('.partners-reads__slider').length > 0) {
		var $status = $('.partners-read__slider-counter');
		var $slickElement = $('.partners-reads__slider');

		$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
		    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
		    var i = (currentSlide ? currentSlide : 0) + 1;
		    $status.text(i + '/' + slick.slideCount);
		});


		$('.partners-reads__slider').slick({
		    slidesToShow: 1,
		    infinite: true,
		    dots: false,
		    arrows: false,
		    slidesToScroll: 1,
		    fade: true,
		    speed: 300,
		    
		});

		$(".partners-reads__slider-btn--prev").on("click", function() {
		    $(".partners-reads__slider").slick("slickPrev");
		});

		$(".partners-reads__slider-btn--next").on("click", function() {
		    $(".partners-reads__slider").slick("slickNext");
		});
	}

	if ($(window).width() <= 860) {
		if($('.about-main__slider').length > 0) {

			$('.about-main__slider').slick({
			    slidesToShow: 1,
			    infinite: false,
			    dots: false,
			    arrows: false,
			    slidesToScroll: 1,
			    centerMode: true,
			    autoplay: true,
			    autoplaySpeed: 2000,
			    centerPadding: '200px',
			    responsive: [
			        {
			            breakpoint: 640,
			            settings: {
			                centerPadding: '46px',
			            }
			        },
			        {
			            breakpoint: 480,
			            settings: {
			            	centerMode: false,
			                centerPadding: '0px',
			            }
			        },
			    ]
			    
			});
		}
	}

	if ($(window).width() <= 760) {
		if($('.items-list__slider').length > 0) {

			$('.items-list__slider').slick({
			    slidesToShow: 1,
			    infinite: false,
			    dots: false,
			    arrows: false,
			    slidesToScroll: 1,
			    centerMode: true,
			    centerPadding: '280px',
			    responsive: [
		        {
		            breakpoint: 640,
		            settings: {
		                centerPadding: '200px',
		            }
		        },
		        {
		            breakpoint: 540,
		            settings: {
		                centerPadding: '144px',
		            }
		        },
		        {
		            breakpoint: 460,
		            settings: {
		                centerPadding: '100px',
		            }
		        },

		        {
		            breakpoint: 380,
		            settings: {
		                centerPadding: '50px',
		            }
		        },

		        {
		            breakpoint: 350,
		            settings: {
		                centerPadding: '40px',
		            }
		        },
		    ]
			    
			});
		}
	}

	if ($(window).width() <= 960) {
		if($('.card-slider').length > 0) {

			$('.card-slider').slick({
			    slidesToShow: 1,
			    infinite: true,
			    dots: true,
			    arrows: false,
			    slidesToScroll: 1,
			});
		}
	}


	var isMenuOpen = false,
		isSearchOpen = false;
	$('.burger-content').on('click', function (e) {
		e.preventDefault();
		var $this = $(this);
		if (isSearchOpen) {
            $('.header__search-wrap').slideUp();
            $('.header__bottom').removeClass('active');
            $('.main-wrap').removeClass('layout');
            $('.header').removeClass('active');
            $('.header__menu-wrap').removeClass('active');
            $('.header__search-content').removeClass('active');
            isSearchOpen = false;
		}
        if ($this.hasClass('active')) {
            $('.header__menu-wrap').slideUp();
            $('.header__bottom').removeClass('active');
            $('.main-wrap').removeClass('layout');
            $('.header').removeClass('active');
            $('.header__search-wrap').removeClass('active');
		} else {
            $('.header__menu-wrap').slideDown();
            $('.header__bottom').addClass('active');
            $('.main-wrap').addClass('layout');
            $('.header').addClass('active');
            $('.header__search-wrap').removeClass('active');
		}
		$this.toggleClass('active');
        isMenuOpen = !isMenuOpen;
	    // $('body').toggleClass('overflowHidden');
	});

	$('.header__search-content').on('click', function (e) {
		e.preventDefault();
        var $this = $(this);
        if (isMenuOpen) {
            $('.header__menu-wrap').slideUp();
            $('.header__bottom').removeClass('active');
            $('.main-wrap').removeClass('layout');
            $('.header').removeClass('active');
            $('.header__search-wrap').removeClass('active');
            $('.burger-content').removeClass('active');
            isMenuOpen = false;
        }
        if ($this.hasClass('active')) {
            $('.header__search-wrap').slideUp();
            $('.header__bottom').removeClass('active');
            $('.main-wrap').removeClass('layout');
            $('.header').removeClass('active');
            $('.header__menu-wrap').removeClass('active');
		} else {
            $('.header__search-wrap').slideDown(function () {
                $('.header__search-field').find('input[name="search"]').focus();
            });
            $('.header__bottom').addClass('active');
            $('.main-wrap').addClass('layout');
            $('.header').addClass('active');
            $('.header__menu-wrap').removeClass('active');
		}
        $this.toggleClass('active');
        isSearchOpen = !isSearchOpen;
	    // $('body').toggleClass('overflowHidden');
	});

	$('.header__search-close').on('click', function (e) {
		e.preventDefault();
	    $('.header__search-wrap').slideUp();
	    $('.header__bottom').toggleClass('active');
	    $('.main-wrap').removeClass('layout');
	    $('.header').removeClass('active');
	    $('.header__menu-wrap').removeClass('active');
	    // $('body').toggleClass('overflowHidden');
	});

	if ($(window).width() < 961) {
		$('.header__menu > ul > li > a').on('click', function (e) {
		    var $this = $(this);
		    if ($this.next('.header__menu-dropdown').length > 0 ) {
		    	e.preventDefault();
		    	$this.toggleClass('is-open');
		    	$this.next('.header__menu-dropdown').slideToggle();
		    }
		})

		$('.header__menu > ul > li > a').each( function (e) {
		    var $this = $(this);
		    if ($this.next('.header__menu-dropdown').length > 0 ) {
		    	$this.addClass('chevron');
		    }
		})
	} 

	$('.dealers__item-header').on('click', function() {
		$(this).toggleClass('active');
  		$(this).next('.dealers__item-content').slideToggle();
	});

	$('.order-goods__item-header').on('click', function() {
		$(this).toggleClass('active');
  		$(this).next('.order-goods__tab-col').slideToggle();
	});

	$('.catalog__flter-btn-mobile').on('click', function() {
		$(this).toggleClass('active');
  		$('.catalog__filter-wrap').slideToggle();
	});

	$('select').selectize();



	$('.jsSwitchModification').on('click', function () {
		var $this = $(this),
			modificationNum = $this.attr('data-modification-num');
		if ($this.hasClass('active')) return false;
        $('.jsSwitchModification').removeClass('active');
        $this.addClass('active');

        var $sliderWrap = $('.card-slider-wrap'),
			$sliders = $sliderWrap.find('.card-slider'),
			$infoWrap = $('.catalog__item-info-wrap'),
			$infoItems = $infoWrap.find('.catalog__item-info'),
			$specsWrap = $('.card__specifications-wrap'),
			$specs = $specsWrap.find('.card__specifications');
        $sliders.hide().removeClass('active');
        $sliders.filter('[data-modification-num='+ modificationNum +']').fadeIn(300).addClass('active');
        $infoItems.hide().removeClass('active');
        $infoItems.filter('[data-modification-num='+ modificationNum +']').fadeIn(300).addClass('active');
        $specs.hide().removeClass('active');
        $specs.filter('[data-modification-num='+ modificationNum +']').fadeIn(300).addClass('active');
        if ($(window).width() <=960) {
        	$sliders.slick('unslick');
            $sliders.filter('[data-modification-num='+ modificationNum +']').slick({
                slidesToShow: 1,
                infinite: true,
                dots: true,
                arrows: false,
                slidesToScroll: 1,
            });
		}
    });





	function customPlaceholder() {
        var $customPlaceholder = $('.custom-placeholder');
		$customPlaceholder.focus(function() {
            var $this = $(this);
            $this.closest('.orders__field').addClass('focus');
            setTimeout(function() {
                $this.closest('.orders__field').addClass('focus-in');
            }, 300);
        });
        $customPlaceholder.focusout(function() {
            var $this = $(this);
            if ($this.val().length===0) {
                $this.closest('.orders__field').removeClass('focus-in');
                $this.closest('.orders__field').addClass('focus-out');
                setTimeout(function() {
                    $this.closest('.orders__field').removeClass('focus');
                    $this.closest('.orders__field').removeClass('focus-out');
                }, 300);
            }
        })
	}
	var $customPlaceholder = $('.custom-placeholder');
	if ($customPlaceholder.length > 0) {
		customPlaceholder();
	}


  
});



































$(document).ready(function () {
    function pageWidget(pages) {
        var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
        widgetWrap.prependTo("body");
        for (var i = 0; i < pages.length; i++) {
            $('<li class="widget_item"><a class="widget_link" href="' + pages[i][0] + '.html' + '">' + pages[i][1] + '</a></li>').appendTo('.widget_list');
        }
        var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:absolute;top:0;left:0;z-index:9999;padding:5px 10px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_list{padding: 0;}.widget_item{list-style:none;padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
        widgetStilization.prependTo(".widget_wrap");
    }

    pageWidget([
        ['index', 'Главная'],
        ['articles', 'Статьи'],
        ['article-single', 'Статья'],
        ['certificate', 'Сертификаты'],
        ['about', 'О нас'],
        ['dealers', 'Дилеры'],
        ['requisites', 'Реквизиты'],
        ['partner', 'Партнер'],
        ['order', 'Заказ'],
        ['feedback', 'Обратная связь'],
        ['contacts', 'Контакты'],
        ['catalog', 'Каталог'],
        ['card', 'Карточка'],
    ]);
});